---
layout: ids_default
searchbar: false
title: "Comment contribuer ?"
lead: "Guide du participant"
nav_order: 3

---

<h2>Comment soumettre un article ?</h2>

Créez un ticket ici: [bitbucket.org/alxfontana/www.inegalitesdesalaires.fr/issues](https://bitbucket.org/alxfontana/www.inegalitesdesalaires.fr/issues?status=new&status=open)

<h2>Comment contribuer ?</h2>

Ce site est en construction... par vous !

Pour contribuer, vous devrez télécharger les sources du site, le modifier sur votre ordinateur, puis m'envoyer une "pull request".
Cela nécessite d'utiliser **Git** et **Jekyll**.

### Quelles sont les tâches à faire ?

Nous maintenons un fichier [Notes.txt](https://bitbucket.org/alxfontana/www.inegalitesdesalaires.fr/src/master/Notes.txt) pour savoir ce qu'il reste à faire.

### Télécharger les sources

Vous avez besoin de connaître "Git" pour travailler ici.

Clonez le repository BitBucket : https://bitbucket.org/alxfontana/www.inegalitesdesalaires.fr

### Modifier les sources

Tout est expliqué dans le fichier README.md sur https://bitbucket.org/alxfontana/www.inegalitesdesalaires.fr:
- Vous devez installer Ruby 2.4.0, puis `gem install --user-install bundler jekyll`,
- Puis faire `bundle install` pour s'assurer que toutes les librairies sont présentes,
- Puis `bundle exec jekyll serve` pour que votre site soit publié sur http://localhost:4000

### Comment s'y retrouver ?

- `/dossiers`: Contient les dossiers thématiques,
- `/archives`: Contient les archives de documents,
- `/contact`: Contient diverses pages annexes.

Vous travaillerez certainement dans les deux premiers dossiers seulement. Pour faire des modifications plus avancées sur le site,
tournez-vous vers les docs de Jekyll.

Quelques astuces :
- Les fichiers ".md" sont en "markdown". Plus précisément, c'est du CommonMark [dont vous pouvez trouver la doc ici](https://commonmark.org/help/).
- Les entêtes de fichiers s'appellent le "front matter". Cherchez "front matter jekyll" pour trouver.
- Dans le front matter, la mention "layout: ids_default" référence le fichier ids_default.html. Facile !
- Ignorez tout ce qui se trouve dans _site, c'est les site construit par Jekyll à partir de vos sources.

### Attention

- Ne pas modifier les URLs d'articles existants, et donc éviter de modifier leur dossier.
  Si nécessaire, utiliser la balise "permalink",
- Ne pas publier de copie de sites, pour une question de copyright.
- Dans votre message de commit, quand vous utilisez `git commit -am "message"`, décrivez clairement ce que vous avez modifié.

### Comemnt renvoyer le code ?

BitBucket, comme GitHub, dispose d'un système de "pull-requests":
- Faites un "fork" du projet sur BitBucket, cela crée un projet dans votre espace personnel BitBucket,
- Mettez à jour le repository Git cloné,
- Dans BitBucket, cliquez sur "Create a pull request" ([la doc d'Atlassian ici](https://www.atlassian.com/git/tutorials/making-a-pull-request)).
- J'essaierai de voir les pull requests, les accepter et les publier, si elles sont suffisamment sérieuses. 

### FAQ

- Pourquoi ne pas utiliser GitHub ?
  GitHub a un historique de participation fallacieuse au féminisme. Citation de la VP of Diversity,
  Nicole Sanchez: "Some of the biggest to progress (of diversity) is white women."
- Pourquoi est-ce aussi compliqué ?
  Tout est compliqué, en informatique. Si j'avais fait un site Wordpress, je n'aurais pas su modifier le HTML pour
  le faire aussi beau. Et surtout, Wordpress oblige à avoir une base de données, a constamment des failles de
  sécurité à cause desquelles il faut mettre à jour, bref, c'est compliqué aussi. L'avantage de Jekyll est qu'il
  construit le site en HTML, donc il sera toujours lisible dans les navigateurs, sans mise à jour ni maintenance.
