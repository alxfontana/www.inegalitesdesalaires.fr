---
layout: ids_default
searchbar: false
title: "Contact"
lead: ""
nav_order: 3

---

### Ce site est le vôtre !

Pour ajouter des documents d'archives et continuer les dossiers thématiques : [Guide du contributeur]({% link contact/contribuer.md %})

### Site publié par

Alexis Fontana. Contact: [contact@inegalitesdesalaires.fr](mailto:contact@inegalitesdesalaires.fr)

### Site hébergé par :
~~~
OVH SAS au capital de 10 069 020 €
RCS Lille Métropole 424 761 419 00045
Code APE 2620Z
Siège social : 2 rue Kellermann - 59100 Roubaix - France 
~~~

### Pas d'avertissement relatif aux cookies

Ce site n'utilise ni cookies ni données personnelles. Le bonheur ! On vous respecte ! Pas comme noustoutes.org !

### Licences

Voir [Licences]({% link contact/remerciements.md %}).