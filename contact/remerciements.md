---
layout: ids_default
searchbar: false
title: "Licences"
lead: ""
nav_order: 3

---

<h2>{{ page.title }}</h2>

- Le template de départ était [Just The Docs](https://pmarsceill.github.io/just-the-docs/), publié sous licence MIT : [LICENSE.txt]({% link contact/LICENSE.txt %})
- Jekyll est sous licence MIT : [https://github.com/jekyll/jekyll/blob/master/LICENSE](https://github.com/jekyll/jekyll/blob/master/LICENSE)
- Lunr est sous licence type MIT : [https://github.com/olivernn/lunr.js/blob/master/LICENSE](https://github.com/olivernn/lunr.js/blob/master/LICENSE)
- Bootstrap 4 est sous licence MIT : [https://github.com/twbs/bootstrap/blob/v4.0.0/LICENSE](https://github.com/twbs/bootstrap/blob/v4.0.0/LICENSE)
- Font Awesome Free 5 est sous plusieurs licences : [https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt](https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)