#!/usr/bin/env bash

# Boilerplate
set -eu
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $SCRIPT_DIR/../archives/youtube

# Actual script

echo
echo "Ce script télécharge automatiquement toutes les vignettes de vidéos, pour tous les fichiers .md situés dans /archives/youtube/*"
echo

FILENAMES="$(grep -l "youtube: \"" -R .)"
for FILENAME in $FILENAMES ;
do
    echo "Extraction du youtube_id de $FILENAME"
    # "grep" tente d'extraire la ligne qui correspond à la regex "youtube *:". En regex, l'étoile "*" signifie "le caractère précédent peut être répété 0 à plusieurs fois.
    # "cut" utilise le délimiteur ":" pour séparer les champs, et affiche le champ numéro 2. En gros, dans "youtube: abc", il affiche " abc"
    # Le second "cut" utilise le délimiteur "double-guilemet". En gros, dans ' "mon_id" ', il affiche "mon_id" (sans les guillemets)
    YOUTUBE_ID="$(grep -E "youtube *:" $FILENAME | cut -d':' -f2 | cut -d'"' -f2)"
    TARGET_FILE="$(dirname $FILENAME)/youtube-thumbnail-$YOUTUBE_ID.jpg"
    if [[ "$YOUTUBE_ID" == "" ]] ; then
        echo "Could not find the Youtube ID in $FILENAME"
    elif [[ -f "$TARGET_FILE" ]] ; then
        echo "Thumbnail $TARGET_FILE already downloaded"
    else
        URL="$(youtube-dl --skip-download --get-thumbnail $YOUTUBE_ID)"
        wget "$URL" -O "$TARGET_FILE"
    fi
done