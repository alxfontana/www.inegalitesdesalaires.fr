#!/usr/bin/env bash

# Boilerplate
set -eu
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $SCRIPT_DIR/../archives/youtube

# Actual script

echo
echo "Ce script créée automatiquement une page pour une vidéo"
echo "Usage: _scripts/archive_video.sh youtube_id"
echo

YOUTUBE_ID="$1"

youtube-dl --skip-download --write-info "$YOUTUBE_ID" -o video-info-tmp

TITLE="$(jq -r ".fulltitle" video-info-tmp.info.json)"
AUTHOR="$(jq -r ".uploader" video-info-tmp.info.json)"
YOUTUBE_DATE="$(jq -r ".upload_date" video-info-tmp.info.json)"
THUMBNAIL_URL="$(jq -r ".thumbnail" video-info-tmp.info.json)"
CHANNEL_URL="$(jq -r ".channel_url" video-info-tmp.info.json)"

# Le "tr" met le nom de l'auteur en minuscules
# Le "sed" applique un remplacement ("s/" = substitute) de la regex [^a-z]+ par des tirets
AUTHOR_FOLDER="$(echo "$AUTHOR" | tr '[:upper:]' '[:lower:]' | sed -E 's/[^a-z]+/-/g')"

# La date youtube est au format 20001231, on veut ajouter des tirets: 2020-12-31
VIDEO_DATE="$(echo "$YOUTUBE_DATE" | sed -E 's/([0-9]{4})([0-9]{2})([0-9]{2})/\1-\2-\3/')"

# Ajoute un caractère d'échappement pour le backslash: \ -> \\
TITLE="$(echo "$TITLE" | sed -E 's/\\/\\\\/g')"
# Ajoute un caractère d'échappement avant les double-guillemets, pour éviter les confusions: " -> \"
TITLE="$(echo "$TITLE" | sed -E 's/"/\\"/g')"

if [[ -z "$AUTHOR_FOLDER" ]] ; then
    echo "L'auteur n'est pas au bon format: \"$AUTHOR\""
    exit 1
fi
if [[ -z "$TITLE" ]] ; then
    echo "Le titre n'est pas au bon format: \"$TITLE\""
    exit 1
fi
if [[ -z "$VIDEO_DATE" ]] ; then
    echo "La date n'est pas au bon format: \"$VIDEO_DATE\" pour $YOUTUBE_DATE"
    exit 1
fi
if [[ ! -d "$AUTHOR_FOLDER" ]] ; then
    mkdir "$AUTHOR_FOLDER"
fi

cat << DELIMITEUR_DE_FIN > "$AUTHOR_FOLDER/${YOUTUBE_ID}.md"
---
layout: "ids_archive"
type: "archive_video"
indexation_auteur: "Youtube"
indexation_page: "$AUTHOR - $TITLE"
indexation_version: "$VIDEO_DATE"
title: "$AUTHOR - $TITLE"
hide_content: true
youtube: "$YOUTUBE_ID"

---


==MARQUEUR==

TODO: Résumé à écrire, transcript à structurer.

==MARQUEUR==

Placer le transcript ici.

DELIMITEUR_DE_FIN

rm video-info-tmp.info.json

echo
echo "Fait. Pensez à écrire le transcript."
echo "$AUTHOR_FOLDER/${YOUTUBE_ID}.md"