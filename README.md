
# Comment participer ?

Téléchargez le repository Git:

~~~bash
git clone git@bitbucket.org:alxfontana/www.inegalitesdesalaires.fr.git
# Si vous avez déjà installé Ruby, Jekyll et Bundler:
bundle install # Télécharge toutes les dépendances
bundle exec jekyll serve # Permet de servir le site web depuis votre machine : http://localhost:4000
~~~

### Pour les utilisateurs de Mac

Les autres, vous pouvez aller voir pour d'autres OS: [https://jekyllrb.com/docs/installation/macos/]

Installez Git et configurez-le. Si vous ne connaissez pas, ça va être long à apprendre (mais c'est standard dans l'informatique).

Installez Brew (Homebrew).

Installez Ruby :

~~~ bash
brew install ruby rbenv
# Ruby must be >2.4.0
ruby --version
# Check with:
/usr/local/Cellar/ruby/2.6.5/bin/ruby --version
~~~

Edit your ~/.profile:
~~~ bash
export PATH=/usr/local/Cellar/ruby/2.6.5/bin:$PATH
export RBENV_ROOT=/usr/local/var/homebrew/linked/rbenv
eval "$(rbenv init -)"
~~~

Then run:
~~~ bash
rbenv install 2.6.3
rbenv global 2.6.3
gem install --user-install bundler -v '2.0.1'
gem install --user-install jekyll
~~~

### Running the website

~~~ bash
bundle install # The first time, to download dependencies
bundle exec jekyll --version # To know the version, 3.8.6
bundle exec jekyll serve # To serve the blog from your machine - http://localhost:4000
~~~


# Comment renvoyer votre contribution ?

C'est compliqué, parce que mon BitBucket est en lecture seule. Il vous faut forker le projet,
puis faire un git-push de votre branche sur votre espace BitBucket, puis me créer une pull-request.

# Ressources

Le README du template just-the-docs est disponible à : [https://github.com/pmarsceill/just-the-docs/blob/master/README.md](https://github.com/pmarsceill/just-the-docs/blob/master/README.md).

Les docs d'installation de Jekyll sont disponibles à : [https://jekyllrb.com/docs/installation/](https://jekyllrb.com/docs/installation/).