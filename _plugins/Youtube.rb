module Jekyll
  ###
  #
  # Cette classe permet de définir le tag {% youtube "youtubeid" %}
  # Résultat: <iframe> de la vidéo
  #
  ###
  class YoutubeTag < Liquid::Tag

    def initialize(tag_name, markup, tokens)
      super

      @markup = markup
    end

    def render(context)
      url = get_value(context, @markup.strip)

      if url =~ /https:\/\/www.youtube.com\/watch\?v=([a-zA-Z0-9_-]+)/ then
        @youtubeid = $1
      elsif url =~ /https:\/\/www.youtube.com\/embed\/([a-zA-Z0-9_-]+)/ then
        @youtubeid = $1
      elsif url =~ /https:\/\/youtu.be\/([a-zA-Z0-9_-]+)/ then
        @youtubeid = $1
      elsif url =~ /^([a-zA-Z0-9_-]+)$/ then
        @youtubeid = $1
      else
        printf "Youtube URL unrecognized: " + url
        raise "Youtube URL unrecognized: " + url
      end

      printf "Youtube macro for https://www.youtube.com/embed/#{ @youtubeid }\n"
      "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/#{ @youtubeid }\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"
    end

    # Unfortunately, Jekyll passes the markup in the first parameter of the constructor instead of the value, so we need to extract its value
    def get_value(context, expression)
      if (expression[0]=='"' and expression[-1]=='"') or (expression[0]=="'" and expression[-1]=="'")
        # it is a literal
        return expression[1..-2]
      else
        # it is a variable
        lookup_path = expression.split('.')
        result = context
        lookup_path.each do |variable|
          result = result[variable] if result
        end
        return result
      end
    end
  end
end

Liquid::Template.register_tag('youtube', Jekyll::YoutubeTag)