module Jekyll
  ###
  #
  # Cette classe définit le tag {% ids_ref archives/chemin/fichier.md [mode=default|title|asterisk] %}
  # qui permet de citer un dossier ou une archive.
  #
  # Le |mode est optionnel
  #
  ###
  class IDS_RefTag < Liquid::Tag

    def initialize(tag_name, content, tokens)
      super
      @content = content.strip()
    end

    # Le rendu est similaire à un {% link %}
    # Voir doc du LinkTag: https://github.com/jekyll/jekyll/blob/3.6-stable/lib/jekyll/tags/link.rb
    def render(context)
      rendered_input = Liquid::Template.parse(@content).render(context).split(" ")
      provided_url = rendered_input[0]
      mode = rendered_input.size() >= 2 ? rendered_input[1].delete_prefix("mode=") : "default"
      mode = mode.delete_prefix("\"").delete_suffix("\"")

      page = get_page(context, provided_url)


      if mode == "asterisk"
        # Crée le numéro de référence. C'est un numéro pour chaque item de la page.
        count = context.registers[:page]["ids_ref_tag_internal_variable_count"]
        count = count == nil ? 1 : count + 1
        context.registers[:page]["ids_ref_tag_internal_variable_count"] = count
        return "<sup><a href=\"" + page.url + "\" title=\"" + CGI::escapeHTML(page.data["title"]) + "\">[#{count}]</a></sup>"
      elsif mode == "title"
        label = page.data["title"]
        return "<a href=\"" + page.url + "\">" + label + "</a>"
      elsif page.data["layout"] == "ids_dossier"
        label = "dossier"
      elsif page.data["layout"] == "ids_archive"
        label = "document"
      else
        raise ArgumentError, "Type de document inconnu: '" + page.data["layout"] + "'"
      end
      return "(<a href=\"" + page.url + "\">" + label + "</a>)"
    end

    def get_page(context, provided_url)
      context.registers[:site].each_site_file do |item|
        if item.relative_path == provided_url or item.relative_path == "/#{provided_url}"
          # This takes care of the case for static files that have a leading /
          return item
        end
      end

      raise ArgumentError, "Impossible de trouver le document '#{provided_url}' dans le tag '{% ids_ref %}'."
    end
  end
end

Liquid::Template.register_tag('ids_ref', Jekyll::IDS_RefTag)