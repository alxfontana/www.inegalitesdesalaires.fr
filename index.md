---
layout: ids_base
searchbar: false
lead: "Au-delà des clichés !" # Ou "Les décodeurs !"
title: "Inégalités de salaires"
nav_order: 1
permalink: /

---

{% comment %}
<section class="topics">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="topics-wrapper border-style">
                        <h3><span class="icon icon-circle-o text-blue"></span>Ouverture</h3>
                        <ul class="topics-list">
                            {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "beginner" | sort:"nav_order" -%}
                            {%- for node in pages_list limit: 5 -%}
                                    <li><a href="{{ node.url | absolute_url }}">{{ node.title }}</a></li>
                            {%- endfor -%}
                        </ul>
                        <ul class="topics-meta">
                            <li>15 Topics</li>
                            <li>Level - Medium</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="topics-wrapper border-style">
                        <h3><span class="icon icon-circle-o text-blue"></span>Avancé</h3>
                        <ul class="topics-list">
                            {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "medium" | sort:"nav_order" -%}
                            {%- for node in pages_list limit: 5 -%}
                                    <li><a href="{{ node.url | absolute_url }}">{{ node.title }}</a></li>
                            {%- endfor -%}
                        </ul>
                        <ul class="topics-meta">
                            <li>15 Topics</li>
                            <li>Level - Medium</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="topics-wrapper border-style">
                        <h3><span class="icon icon-circle-o text-green"></span>Archives</h3>
                        <ul class="topics-list">
                            <li><a href="single.html"> How to use this documentation? </a></li>
                            <li><a href="single.html"> How to find topics? </a></li>
                            <li><a href="single.html"> What is included and why? </a></li>
                            <li><a href="single.html"> Basic knowledge requirments. </a></li>
                            <li><a href="single.html"> Getting Started &amp; What is next. </a></li>
                        </ul>
                        <ul class="topics-meta">
                            <li>15 Topics</li>
                            <li>Level - Medium</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> {% endcomment %}

{% comment %} Plus d'infos sur le formattage ci-dessous: https://getbootstrap.com/docs/4.1/examples/pricing/ {% endcomment %}
<div class="container">
    <div class="card-deck mb-3 ">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Ouverture</h4>
            </div>
            <div class="card-body">
                <p>On vous dit tout sur les arguments basiques.</p>
                <a href="{% link archives/amateur/youtube/osalnef/pP6Rp_V-8qk.md %}">
                    <img src="{% link archives/amateur/youtube/osalnef/youtube-thumbnail-pP6Rp_V-8qk.jpg %}" alt="Vidéo - Osalnef - Pour en finir avec le mythe de l'écart salarial" />
                </a>
                <ul class="list-unstyled mt-3 mb-4">
                    {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "beginner" | sort:"nav_order" -%}
                    {%- for node in pages_list limit: 5 -%}
                            <li class="mb-1">"<a href="{{ node.url | absolute_url }}">{{ node.title }}</a>"</li>
                    {%- endfor -%}
                </ul>
                <a href="{% link categorie_dossiers.md %}" class="btn btn-lg btn-block btn-outline-primary">Je découvre</a>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Avancé</h4>
            </div>
            <div class="card-body">
                <p>Des analyses nécessitant un niveau avancé.</p>
                <a href="{% link archives/amateur/youtube/osalnef/ld9cuo-z6UA.md %}">
                    <img src="{% link archives/amateur/youtube/osalnef/youtube-thumbnail-ld9cuo-z6UA.jpg %}" alt="Vidéo - Osalnef - Pour en finir avec le mythe du plafond de verre" />
                </a>
                <ul class="list-unstyled mt-3 mb-4">
                    {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "medium" | sort:"nav_order" -%}
                    {%- for node in pages_list limit: 5 -%}
                            <li class="mb-1">"<a href="{{ node.url | absolute_url }}">{{ node.title }}</a>"</li>
                    {%- endfor -%}
                </ul>
                <a href="{% link categorie_dossiers.md %}" class="btn btn-lg btn-block btn-primary">J'enquête</a>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Archives</h4>
            </div>
            <div class="card-body">
                <p>Tous les documents archivés pour votre information.</p>
                <ul class="list-unstyled mt-3 mb-4">
                    <li>30 users included</li>
                    <li>15 GB of storage</li>
                    <li>Phone and email support</li>
                    <li>Help center access</li>
                </ul>
                <a href="{% link categorie_archives.md %}" class="btn btn-lg btn-block btn-primary">Accès</a>
            </div>
        </div>
    </div>
</div>