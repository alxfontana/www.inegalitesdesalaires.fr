---
layout: ids_default
searchbar: false
title: "Archives"
lead: "Archives des documents cités"
nav_order: 1
permalink: /archives
---

<h2>{{ page.title }}</h2>

<ul>
{%- assign autorites = "institutionnel,media,academique,amateur," | split: ',' -%}
{%- for autorite in autorites -%}
    {% comment %}<h2>Autorité : "{{ autorite }}"</h2>{% endcomment %}
    {% if autorite == "amateur" %}
        </ul><h2>Autres archives</h2><ul>
    {% endif %}
    
    {%- assign pages_par_auteur = site.html_pages | where: "indexation_autorite", autorite | group_by: "indexation_auteur" -%}
    {%- for site in pages_par_auteur -%}
        
        <li>
            {% comment %} NOM DU SITE {% endcomment %}
            {% if site.name != "" %}
                {{ site.name }}
            {% else %}
                Autres sites
            {% endif %}
            <ul>
                {%- assign noms_de_pages = site.items | group_by: "indexation_page" -%}
                {%- for nom_de_page in noms_de_pages -%}
                    {% if nom_de_page.items.size == 1 %}
                        <li>
                            {% comment %} On affiche la page directement, sans lister les versions. {% endcomment %}
                            {%- for page_version in nom_de_page.items -%}
                                <a href="{{ page_version.url | absolute_url }}" class="navigation-list-link">
                                    {% if nom_de_page.name != "" %}
                                        {{ nom_de_page.name }}
                                    {% else %}
                                        (Pas sans balise indexation_page)
                                    {% endif %}
                                </a>
                            {%- endfor - %}
                        </li>
                        
                    {% else %}
                        <li>
                            {% comment %} On affiche la page, puis chaque version. {% endcomment %}
                            {{ nom_de_page.name }}
                            <ul>
                                {%- for page_version in nom_de_page.items -%}
                                    <li><a href="{{ page_version.url | absolute_url }}" class="navigation-list-link">{{ page_version.indexation_version }}</a></li>
                                {%- endfor - %}
                            </ul>
                        </li>
                    {% endif %}
                {%- endfor -%}
            </ul>
        </li>
    {%- endfor -%}
{%- endfor -%}
</ul>