---
layout: ids_archive
type: archive_page
indexation_auteur: "Recherche"
indexation_page: "Further understanding incivility in the workplace: The effects of gender, agency, and communion."
indexation_version: 2018
indexation_autorite: academique
title: "Further understanding incivility in the workplace: The effects of gender, agency, and communion."
archive_page_url: "https://psycnet.apa.org/doiLanding?doi=10.1037%2Fapl0000289"
#image: 
#pdf: doi-10.1177-0149206314539348.pdf
hide_content: true
other:
 "doi": "10.1037/apl0000289" 
 "doi (url)": "https://doi.org/10.1037/apl0000289"
 "Auteurs": "Gabriel, Allison S.,Butts, Marcus M.,Yuan, Zhenyu,Rosen, Rebecca L.,Sliter, Michael T."
---


==MARQUEUR==

Première étude (pas sûr, mais admettons) à étudier les auteurs d'incivilités au travail (N>1 300).

Conclusions :
- Les femmes subissent + d'incivilités, 
- Incivilités commises principalement par des femmes,
- Plus particulièrement destinées aux femmes ambitieuses. 

Ces résultats répliquent ceux Lim (2008) https://psycnet.apa.org/record/2008-00266-007 au sujet des victimes,
mais pas des agresseurs. Lim, malheureusement, sortait l'idée que les hommes étaient les principaux
agresseurs de son cul, puisque son étude ne prenait pas cet aspect en compte.

Approche théorique du phénomène ici : Sheppard and Aquino (2017)
https://journals.sagepub.com/doi/abs/10.1177/0149206314539348 (surestimation probable du phénomène
qui est certainement du même ordre de grandeur entre les hommes) - que nous avons archivée:
 {% ids_ref archives/academique/journals.sagepub.com/doi-10.1177-0149206314539348.md %}

==MARQUEUR==

Abstract

Research conducted on workplace incivility—a low intensity form of deviant behavior—has generally shown that women 
report higher levels of incivility at work. However, to date, it is unclear as to whether women are primarily treated 
uncivilly by men (i.e., members of the socially dominant group/out-group) or other women (i.e., members of in-group) 
in organizations. In light of different theorizing surrounding gender and incivility, we examine whether women 
experience increased incivility from other women or men, and whether this effect is amplified for women who exhibit 
higher agency and less communion at work given that these traits and behaviors violate stereotypical gender norms. 
Across three complementary studies, results indicate that women report experiencing more incivility from other women 
than from men, with this effect being amplified for women who are more agentic at work. Further, agentic women who 
experience increased female-instigated incivility from their coworkers report lower well-being (job satisfaction, 
psychological vitality) and increased work withdrawal (turnover intentions). Theoretical implications tied to gender 
and incivility are discussed. (APA PsycInfo Database Record (c) 2018 APA, all rights reserved)