---
layout: ids_archive
type: archive_page
indexation_auteur: "Recherche"
indexation_page: "Sisters at Arms: A Theory of Female Same-Sex Conflict and Its Problematization in Organizations "
indexation_version: 2014
indexation_autorite: academique
title: "Sisters at Arms: A Theory of Female Same-Sex Conflict and Its Problematization in Organizations "
archive_page_url: "https://journals.sagepub.com/doi/abs/10.1177/0149206314539348"
#image: 
pdf: doi-10.1177-0149206314539348.pdf
hide_content: true
other:
 "doi": "10.1177/0149206314539348" 
 "doi (url)": "https://doi.org/10.1177/0149206314539348"
 "Auteurs": "Leah D. Sheppard, Karl Aquino"
---


==MARQUEUR==


A RETIRER c'est cité par doi.org/10.1037/apl0000289
Première étude (pas sûr, mais admettons) à étudier les auteurs d'incivilités au travail (N>1 300).

Conclusions :
- Les femmes subissent + d'incivilités, 
- Incivilités commises principalement par des femmes,
- Plus particulièrement destinées aux femmes ambitieuses. 

Ces résultats répliquent ceux Lim (2008) https://psycnet.apa.org/record/2008-00266-007 au sujet des victimes,
mais pas des agresseurs. Lim, malheureusement, sortait l'idée que les hommes étaient les principaux
agresseurs de son cul, puisque son étude ne prenait pas cet aspect en compte.

Approche théorique du phénomène ici : Sheppard and Aquino (2017)
https://journals.sagepub.com/doi/abs/10.1177/0149206314539348 (surestimation probable du phénomène
qui est certainement du même ordre de grandeur entre les hommes).

==MARQUEUR==

Abstract

