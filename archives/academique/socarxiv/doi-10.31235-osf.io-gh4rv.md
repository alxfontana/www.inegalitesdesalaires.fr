---
layout: ids_archive
type: archive_page
indexation_auteur: "Recherche"
indexation_page: "No evidence of any systematic bias against manuscripts by women in the peer review process of 145 scholarly journals"
#indexation_version: 2020
indexation_autorite: academique
title: "No evidence of any systematic bias against manuscripts by women in the peer review process of 145 scholarly journals"
archive_page_url: https://osf.io/preprints/socarxiv/gh4rv/
#image: 
pdf: doi-10.31235-osf.io-gh4rv.pdf
hide_content: true
other:
 "doi": 10.31235/osf.io/gh4rv
 "doi (url)": https://doi.org/10.31235/osf.io/gh4rv
 "Auteurs": "Flaminio Squazzoni Giangiacomo Bravo Pierpaolo Dondio Mike Farjam Ana Marusic Bahar Mehmani Michael Willis Aliaksandr Birukou Francisco Grimaldo"
---


==MARQUEUR==

Conclusions : 
- Pas de discrimination.
- Phénomène d'homophilie.
- Léger avantage pour les femmes en apparence dans certains cas, mais très probablement non-significatif.

==MARQUEUR==

Voir PDF.

Découvert par https://twitter.com/osalnef/status/1247982442666958849?s=20