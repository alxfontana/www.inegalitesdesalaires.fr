---
layout: ids_archive
type: archive_page
indexation_auteur: "Recherche"
indexation_page: "Teaching accreditation exams reveal grading biases favor women in male-dominated disciplines in France"
#indexation_version: 2020
indexation_autorite: academique
title: "Teaching accreditation exams reveal grading biases favor women in male-dominated disciplines in France"
archive_page_url: https://science.sciencemag.org/content/353/6298/474.long
#image: 
pdf: doi-10.1126.science.aaf4372.pdf
hide_content: true
other:
 "doi": "10.1126/science.aaf4372" 
 "doi (url)": "https://doi.org/10.1126/science.aaf4372"
 "Auteurs": "     Flaminio Squazzoni Giangiacomo Bravo Pierpaolo Dondio Mike Farjam Ana Marusic Bahar Mehmani Michael Willis Aliaksandr Birukou Francisco Grimaldo"
---


==MARQUEUR==

Conclusions : 
- Pas de discrimination.
- Phénomène d'homophilie.
- Léger avantage pour les femmes en apparence dans certains cas, mais très probablement non-significatif.

==MARQUEUR==

Abstract.

Discrimination against women is seen as one of the possible causes behind their underrepresentation in certain STEM (science, technology, engineering, and mathematics) subjects. We show that this is not the case for the competitive exams used to recruit almost all French secondary and postsecondary teachers and professors. Comparisons of oral non–gender-blind tests with written gender-blind tests for about 100,000 individuals observed in 11 different fields over the period 2006–2013 reveal a bias in favor of women that is strongly increasing with the extent of a field’s male-domination. This bias turns from 3 to 5 percentile ranks for men in literature and foreign languages to about 10 percentile ranks for women in math, physics, or philosophy. These findings have implications for the debate over what interventions are appropriate to increase the representation of women in fields in which they are currently underrepresented.