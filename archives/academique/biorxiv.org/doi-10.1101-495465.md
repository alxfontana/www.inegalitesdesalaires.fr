---
layout: ids_archive
type: archive_page
indexation_auteur: "Recherche"
indexation_page: "The Case For and Against Double-blind Reviews"
indexation_version: 2018
indexation_autorite: academique
title: "The Case For and Against Double-blind Reviews"
archive_page_url: https://www.biorxiv.org/content/10.1101/495465v1
#image: 
pdf: doi-10.1101-495465.pdf
hide_content: true
other:
 "doi": "10.1101/495465" 
 "doi (url)": "https://doi.org/10.1101/495465"
 "Auteurs": "Amelia R. Cox, Robert Mongomerie"
---


==MARQUEUR==

tude sur 4865 chercheur(e)s étalés sur 8 années, 2 disciplines et 2 journaux. 

Conclusion : Pas de biais.

Il faut + prendre en compte le nombre de journaux (2 seulement) que de chercheurs puisqu'on parle de plafond de verre.
Le nombre élevé permet juste de s'assurer qu'au sein de ces deux journaux, il n'y a pas de plafond de verre.

==MARQUEUR==

Abstract

To date, the majority of authors on scientific publications have been men. While much of this gender bias can be explained by historic sexism and discrimination, there is concern that women may still be disadvantaged by the peer review process if reviewers’ unconscious biases lead them to reject publications with female authors more often. One potential solution to this perceived gender bias in the reviewing process is for journals to adopt double-blind reviews whereby neither the authors nor the reviewers are aware of each other’s identities and genders. To test the efficacy of double-blind reviews, we assigned gender to every authorship of every paper published in 5 different journals with different peer review processes (double-blind vs. single blind) and subject matter (birds vs. behavioral ecology) from 2010-2018 (n = 4865 papers). While female authorships comprised only 35% of the total, the double-blind journal Behavioral Ecology did not have more female authorships than its single-blind counterparts. Interestingly, the incidence of female authorship is higher at behavioral ecology journals (Behavioral Ecology and Behavioral Ecology and Sociobiology) than in the ornithology journals (Auk, Condor, Ibis), for papers on all topics as well as those on birds. These analyses suggest that double-blind review does not currently increase the incidence of female authorship in the journals studied here. We conclude, at least for these journals, that double-blind review does not benefit female authors and may, in the long run, be detrimental.