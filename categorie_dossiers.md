---
layout: ids_default
searchbar: false
title: "Dossiers"
nav_order: 1
lead: "Les dossiers thématiques"
permalink: /dossiers
---

<h2>{{ page.title }}</h2>

<p>
    Ces titres sont les arguments pro-féminins les plus fréquemment entendus dans les documents du ministère ou sur France Télévisions.
    L'objectif est de fournir des faits pour analyser ces opinions.
</p>

<ul>
    {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "beginner" | sort:"nav_order" -%}
    {%- for node in pages_list -%}
        <li class="navigation-list-subitem"><a href="{{ node.url | absolute_url }}" class="navigation-list-link{% if page.url == node.url %} active{% endif %}">{{ node.title }}</a></li>
    {%- endfor -%}
</ul>

<h2>Niveau avancé</h2>

<ul>
    {%- assign pages_list = site.html_pages | where: "layout", "ids_dossier" | where: "niveau", "medium" | sort:"nav_order" -%}
    {%- for node in pages_list -%}
        <li class="navigation-list-subitem"><a href="{{ node.url | absolute_url }}" class="navigation-list-link{% if page.url == node.url %} active{% endif %}">{{ node.title }}</a></li>
    {%- endfor -%}
</ul>