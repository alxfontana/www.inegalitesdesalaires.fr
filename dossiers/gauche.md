---
layout: ids_dossier
title: Le féminisme n'est pas spécialement de gauche
short_title: De gauche ?
niveau: medium
nav_order: 5
nav_order: 100
---

- Mouvements féministes de droite, certes, mais leurs revendications restent égoïstes,
- Beaucoup de féministes ne croient pas à la méritocratie ni au capitalisme,
- Techniques du marxisme.
