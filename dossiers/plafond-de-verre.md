---
layout: ids_dossier
title: "Le plafond de verre : les femmes ne peuvent accéder à certains travails"
short_title: Plafond de verre
niveau: beginner
nav_order: 3
---


La mère de Bill Gates était n°2 d'IBM. Elles avaient la formation, les ressources, l'argent et l'héritage.
