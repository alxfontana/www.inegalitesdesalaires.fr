---
layout: ids_dossier
# Entre 9 et 25% d'inégalité salariale : qu'est-ce que ça veut dire ?
# La discrimination est-elle vraiment de 25%?
title: "Entre 9 et 25% d'inégalité salariale : qu'est-ce que ça veut dire ?"
short_title: Écart inexpliqué
niveau: beginner
nav_order: 1
---

L'"Observatoire des Inégalités" {% ids_ref archives/institutionnel/inegalites.fr/etat-des-lieux/20200416.md mode="asterisk" %} indique: 

> Si l’on tient compte des différences de tranches d’âge, de type de contrat, de temps de travail, de secteur d’activité et de taille d’entreprise,
  il reste un écart moyen de salaire entre les femmes et les hommes d’environ 10,5 %, selon les données du ministère du Travail.
> Cet écart « toutes choses égales par ailleurs » est ce que l’on n’arrive pas à expliquer.

## Peut-on expliquer cet "écart inexpliqué" ?

En bon scientifiques, nommons les critères que l'Observatoire des Inégalités n'a pas cités :
- Les heures supplémentaires non déclarées,
  - Les heures supplémentaires non déclarées pour l'employeur,
  - Les heures supplémentaires non déclarées pour formation personnelle (changement de travail, motivation individuelle, passion pour le domaine),
  - Les heures supplémentaires non déclarées depuis l'enfance. En effet, un enfant passionné de programmation fera un bien meilleur expert.
- Le poste : Si l'étude prend en compte le type de contrat et le secteur d'activité, elle ne prend pas en compte le poste,
  - A poste égal sur le papier, le poste n'est pas égal dans la réalité,
- La productivité :
  - A heures égales, une heure travaillée par un homme et une femme peuvent être très différente,
  - La personne gagnant le plus dans la famille est souvent celle qui reçoit toute la pression pour gagner l'argent du foyer, et s'efforcera donc d'être productive.
  - Un jour par mois, il peut y avoir des différences collectives.
- La motivation et la passion influent.

## Les femmes gagnent plus que les hommes aux États-Unis

TODO : Retrouver l'étude.

Cet article est une ébauche. Il est amené à être progressivement annoté pour citer les sources et leurs facteurs d'influence. [Comment contribuer ?]({% link contact/contribuer.md %})