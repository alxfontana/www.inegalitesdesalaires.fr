---
layout: ids_dossier
title: Les hommes ne sont pas battus
short_title: Hommes battus
niveau: medium
nav_order: 9
nav_order: 100
---

... ou quand ils le sont, ce n'est pas suffisamment nombreux pour être mesuré.

... ou quand ils le sont, ça ne fait pas mal.

