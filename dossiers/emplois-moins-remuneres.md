---
layout: ids_dossier
title: Les femmes occupent des emplois moins bien rémunérés.
short_title: Types d'emplois
niveau: beginner
nav_order: 2
---

Question de choix. Si tu construis un empire, t'es bien rémunérée. Si tu veux faire ce qu'on te demande, et que tu as besoin d'une fiche de poste, pas bien rémunérée.

- L'"Observatoire des Inégalités"{% ids_ref archives/institutionnel/inegalites.fr/des-parcours-differencies/20200417.md %} montre qu'il y a une plus grande proportion de femmes dans les prépas littéraires que scientifiques.