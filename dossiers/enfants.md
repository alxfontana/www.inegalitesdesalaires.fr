---
layout: ids_dossier
title: Le féminisme pense d'abord aux enfants
short_title: Enfants
niveau: medium
nav_order: 1
---

- Le mouvement des Simones pédophiles,
- Les enfants transgenre,
- Breivik, les enfants abandonnés.
