---
layout: ids_dossier
title: Les femmes tentent plus de se suicider.
short_title: Suicide
niveau: medium
nav_order: 8
---

- 3x plus de tentatives
- 3x moins de décès: 7500 hommes pour 2500 femmes.

"Parce qu'elles utilisent des moyens plus violents."
