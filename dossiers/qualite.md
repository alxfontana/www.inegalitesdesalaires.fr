---
layout: ids_dossier
title: "On ne sait pas mesurer la qualité du travail des femmes"
short_title: "Qualité"
niveau: beginner
nav_order: 100
---

Les hommes non plus.

Au final, on a inventé un système universel : le capitalisme. Si tu vends ton travail, c'est qu'il doit être utile.
Si personne n'en veut, c'est que t'es nul. Ça vaut aussi pour les hommes.

Mais comme la majorité des féministes sont aussi contre le capitalisme...


...j'entends dans mon oreille droite que les féministes sont aussi contre la méritocratie. Comme quoi. Elles veulent juste que l'argent tombe, quoi. 

