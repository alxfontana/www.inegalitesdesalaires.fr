---
layout: ids_dossier
title: Les femmes s'occupent plus des tâches domestiques
short_title: Tâches domestiques
niveau: beginner
nav_order: 100
---

Tâches domestiques, selon les féministes :
- Faire un câlin aux enfants avant de les coucher,
- Faire la vaisselle
- Passer l'aspirateur

Tâches non-domestiques, selon les féministes :
- Conduire la voiture chez le garagiste,
- Rénover la salle de bains,
- Gagner l'argent qui permet d'avoir une belle maison.

