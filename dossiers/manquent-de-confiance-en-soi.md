---
layout: ids_dossier
title: Les femmes manquent de confiance en elles
short_title: Confiance en soi
niveau: beginner
nav_order: 100
---

Cet argument prend le plus souvent deux formes:
- Les femmes manquent de confiance en elles pour demander une augmentation,
- Les femmes manquent de confiance en elles pour avoir de l'ambition.

