---
layout: ids_dossier
title: "Les féministes se battent pour l'égalité"
short_title: "C'est l'égalité !"
niveau: beginner
nav_order: 7
---

- Les hommes meurent 3x plus du suicide que les femmes {% ids_ref dossiers/suicide.md %}.
- Les meurent 12x plus d'accidents du travail que les femmes {% ids_ref dossiers/accidents-du-travail.md %}.
- Le décrochage scolaire concerne principalement les garçons {% ids_ref dossiers/scolarite.md %}.