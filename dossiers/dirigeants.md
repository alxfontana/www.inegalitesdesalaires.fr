---
layout: ids_dossier
title: 90% des hauts dirigeants sont des hommes
short_title: 90% Dirigeants
niveau: beginner
nav_order: 100
---

- Pas une preuve de patriarcat,
- Peut-être est-ce une preuve d'intelligence que de rester en bas,

Mais surtout, cela repose sur la théorie qu'une femme sera dirigée plus conformément à ses besoins par une femme:
- Donc un homme sera dirigé plus conformément à ses besoins par un homme,
- Les femmes dirigeantes ne sont pas magiquement meilleures. (cf Thatcher, MLP, médiéval)
- Les femmes sont souvent mieux protégées par un homme au pouvoir.


