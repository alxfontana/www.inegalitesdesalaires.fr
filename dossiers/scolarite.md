---
layout: ids_dossier
title: "Scolarité"
short_title: "Scolarité"
niveau: mra
nav_order: 7
---

TODO : Statistiques concernant le décrochage scolaire.
## Collège

Les garçons sont discriminés :
- En mathématiques, les garçons reçoivent des notes 6% inférieures aux filles pour la même copie. {% ids_ref archives/media/bbc/marking-bias-32302022/20150414.md mode="asterisk" %}
- Il existe d'autres études pour les autres matières, votre contribution est bienvenue.

## Université

Source : {% ids_ref archives/institutionnel/inegalites.fr/des-parcours-differencies/20200417.md mode="title" %}

![image]({% link archives/institutionnel/inegalites.fr/des-parcours-differencies/part-des-garcons-a-luniversite.jpeg %})